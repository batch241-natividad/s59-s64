import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId, userId } = useParams();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [totalAmount, setTotalAmount] = useState(1);

  const createOrder = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        userId: userId,
        productId: productId,
        totalAmount: totalAmount
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Order confirmed',
            icon: 'success',
            text: 'You have successfully ordered this item.',
          });
          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  return (
   <Container>
  <Row className="justify-content-center">
    <Col xs={12} md={8} lg={6}>
      <Card>
        <Card.Body className="text-center">
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          <Card.Subtitle>Quantity:</Card.Subtitle>
          <Form.Control
            type="number"
            value={totalAmount}
            onChange={(e) => setTotalAmount(e.target.value)}
          />

          {user.id !== null ? (
            <Button variant="primary" onClick={() => createOrder(productId)}>
              Create Order
            </Button>
          ) : (
            <Button className="btn btn-danger" as={Link} to="/login">
              Log in to create Order
            </Button>
          )}
        </Card.Body>
      </Card>
    </Col>
  </Row>
</Container>

  );
}