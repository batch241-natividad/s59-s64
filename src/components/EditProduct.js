import { useState, useEffect, useContext } from 'react';
import {  useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import {  Form, Button } from 'react-bootstrap';

export default function ProductEdit() {
  const { productId } = useParams();
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  const [product, setProduct] = useState(null);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

 function editProduct(e) {
  e.preventDefault();

  if (!product || !product._id) {
    console.error("Invalid product object");
    return;
  }

  // Check if user is an admin
  if (user.isAdmin) {
    fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}/edit`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Product successfully updated",
            icon: "success",
            text: `${name} is now updated`,
          });

          navigate("/DashBoardProduct");
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: `Something went wrong. Please try again later!`,
          });
        }
      })
      .catch((err) => {
        console.error(err);
        Swal.fire({
          title: "Error!",
          icon: "error",
          text: `Something went wrong. Please try again later!`,
        });
      });
  } else {
    console.error("User is not an admin");
    // Show an error message or disable the update functionality
  }
}


    return (
    <div className="ProductEdit">
      <h1>Edit Product</h1>
      {product && (
        <Form onSubmit={editProduct}>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              className="form-control"
              id="description"
              rows="3"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
          </div>
          <div className="form-group">
            <label htmlFor="price">Price</label>
            <input
              type="number"
              className="form-control"
              id="price"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
          <Button type="submit" className="btn btn-primary">
            Update
          </Button>
        </Form>
      )}
    </div>
  );
}