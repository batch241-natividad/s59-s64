import { Carousel } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.freepngimg.com/thumb/tool/86771-viking-weapon-cold-sword-png-download-free.png"
          alt="Sword"
          style={{ width: "400px", height: "400px" }}
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://vignette.wikia.nocookie.net/fallout/images/e/e7/SpearFNV.png/revision/latest/scale-to-width-down/2000?cb=20110210215242"
          alt="Spear"
          style={{ width: "400px", height: "400px" }}
        />
        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Aliquam sit amet gravida nibh, facilisis gravida odio.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://vignette.wikia.nocookie.net/assassinscreed/images/d/db/ACU_Morningstar.png/revision/latest?cb=20171015144945"
          alt="Morningstar"
          style={{ width: "400px", height: "400px" }}
        />
        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}
