import {useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar() {


  const { user } = useContext(UserContext);

  return (
   <Navbar bg="light" expand="lg" className="sticky-top">
  <Navbar.Brand as={Link} to="/Home">Weapon Shop</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="ml-auto">
      <Nav.Link as={NavLink} to="/Home">Home</Nav.Link>
      <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
      { user.id !== null ?
        <>
          { user.isAdmin === true &&
            <NavDropdown title="Dashboard" id="basic-nav-dropdown">
            <NavDropdown.Item as={NavLink} to="./DashBoardProduct"> Retrieve All Products</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to="/create-products"> Retrieve All Products</NavDropdown.Item>
            </NavDropdown>

          }

          <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </>
        :
        <>
          <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
          <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
        </>
      }
    </Nav>
  </Navbar.Collapse>
</Navbar>

  );
}


