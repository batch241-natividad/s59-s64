

import {Link} from 'react-router-dom';

// S50 ACTIVITY
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({product, isAdmin}) {

	const {name, description, price, _id} = product;

	

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	      
	          <Button 
	        	className="bg-primary" 
	        	as={Link} 
	        	to={`/products/${_id}`}
	        	
	        >
	        	Details
	        </Button>
	    </Card.Body>
	</Card>
	)
}
