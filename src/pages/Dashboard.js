import React, { useState } from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import '../App.css';

export default function DashBoard() {
  const [checked, setChecked] = useState(false);
  const [radioValue, setRadioValue] = useState('1');

  const radios = [
  { name: 'Products', value: '1' },
  { name: 'Admins', value: '2' },
  ];

  return (
    <>
    <div className="text-center">
    <ButtonGroup>
    {radios.map((radio, idx) => (
      <>
      <ToggleButton
      key={idx}
      id={`radio-${idx}`}
      type="radio"
      variant={idx % 2 ? 'outline-success' : 'outline-danger'}
      name="radio"
      value={radio.value}
      checked={radioValue === radio.value}
      onChange={(e) => setRadioValue(e.currentTarget.value)}
      >
      {radio.name}
      </ToggleButton>

         {radio.name === "Products" && (
                <DropdownButton id="product-dropdown-button" title="Dropdown button" className="dropdown-button">
                  <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                  <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                </DropdownButton>
              )}
            </>
          ))}
        </ButtonGroup>
      </div>
    </>
  );
}