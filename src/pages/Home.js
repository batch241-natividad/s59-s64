import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Weapon Shop",
		content: "Welcome! In need of armor and weapons? You have come to the right place just click the button below to look on the items you need",
		destination: "/Home",
		label: "Buy now!"
	}

	return (
		<>
			<Banner data={data} />
     		<Highlights />
     		
		</>
	)
}
