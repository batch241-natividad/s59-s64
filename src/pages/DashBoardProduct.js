import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Alert from 'react-bootstrap/Alert';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';



export default function AdminDashboard () {
  const navigate = useNavigate();
  

 const [ updatedProduct, setUpdatedProduct] = useState({});
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [formData, setFormData] = useState({
    name: '',
    price: '',
    description: '',
  });




  const [error, setError] = useState(null);
  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = () => setShowModal(true);
const handleInputChange = (event) => {
  const { name, value } = event.target;
  setFormData({ ...formData, [name]: value });
};


 function handleSubmit(e) {
  e.preventDefault();
  if (user.isAdmin === true) {
    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`

        },
      body: JSON.stringify(formData)
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      if (data) {
          Swal.fire({
            title: 'added a new product',
            icon: 'success',
            text: 'You have successfully added a new product',
          });
          navigate('/home');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      setProducts([...products, data]);
      handleCloseModal();
    })
    .catch(error => {
      console.error(error);
      setError(error.message);


    });
  }
}

  useEffect(() => {
    if (user.isAdmin === true) {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
        method: "GET",
       
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`
        }

      })
        .then(res => res.json())
        .then(data => {
          setProducts(data);
          console.log(data);
          

          
        })
        .catch(error => {
          console.error(error);
          setError(error.message);
        });
    }
  }, [user]);

  if (user.isAdmin === false) {
    return (
      <Alert variant="danger">You do not have access to this page.</Alert>
    );
  }
const handleUpdateProduct = (product) => {
  const updatedProduct = { ...product, isActive: !product.isActive };
  setUpdatedProduct(updatedProduct);
  fetch(`${process.env.REACT_APP_API_URL}/products/${product._id}/archive`, {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`
  },
  body: JSON.stringify({ isActive: updatedProduct.isActive }),

})
    .then((response) => response.json())
    .then((data) => console.log(data))
    .catch((error) => console.log(error));
};


  return (
   <>
  <h1 className="mb-4 text-center">Admin Dashboard</h1>

  <div className="mb-4 d-flex justify-content-center">
    <button className="btn btn-primary" onClick={handleShowModal}>Create Product</button>
  </div>

  {error && <div className="alert alert-danger">{error}</div>}
  
  <div className="table-responsive">
    <h2 className="text-center">Product List</h2>
    <table className="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Description</th>
          <th>Orders</th>
          <th>Availability</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => (
          <tr key={product.id}>
            <td>{product.name}</td>
            <td>{product.price}</td>
            <td>{product.description}</td>
            <td>
              <ul>
                {product.orders.map((order) => (
                  <li key={order.id}>
                    {order.userId} ordered {order.quantity} units
                  </li>
                ))}
              </ul>
            </td>
            <td>{product.isActive ? "Yes" : "No"}</td>
            <td>
              <div class="btn-group" role="group">
                <button class={"btn " + (product.isActive ? "btn-danger" : "btn-success")} onClick={() => handleUpdateProduct(product)}>
                  {product.isActive ? "Disable" : "Enable"}
                </button>
                <a class="btn btn-primary" href={`/products/${product._id}/edit`}>Details</a>
              </div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>

  <div class="table-responsive">
    <h2 class="text-center">Product Availability</h2>
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Availability</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => (
          <tr key={product.id}>
            <td>{product.name}</td>
            <td>{product.availability ? "Yes" : "No"}</td>
            <td>{product.isActive ? "Available" : "Not Available"}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>

  <Modal show={showModal} onHide={handleCloseModal}>
    <Modal.Header closeButton>
      <Modal.Title>Create Product</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <Form method="POST" onSubmit={handleSubmit}>
        <Form.Group controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            name="name"
            value={formData.name}
            onChange={handleInputChange}
          />
        </Form.Group> 
        <Form.Group controlId="price">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            name="price"
            value={formData.price}
            onChange={handleInputChange}
          />
        </Form.Group>
        <Form.Group controlId="description">
          <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                name="description"
                value={formData.description}
                onChange={handleInputChange}
              />
            </Form.Group>

            <Button variant="primary" type="submit" >Create</Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};


